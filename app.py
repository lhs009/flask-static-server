# app.py
import os
from flask import Flask, render_template, request, jsonify


#Flask 객체 인스턴스 생성
#app = Flask(__name__, static_folder='public')

UPLOAD_DIR = "./static/outputs/"

app = Flask(__name__)
app.config['UPLOAD_DIR'] = UPLOAD_DIR



@app.route('/') # 접속하는 url
def index():
  return render_template('index.html')

@app.route('/outputs', methods=['GET'])
def processed_file():
  fileName = request.args.get('f')
  return app.send_static_file('outputs/' + fileName)

@app.route('/upload', methods = ['POST'])
def upload_file():
  f = request.files['image']
  path = os.path.join(app.config['UPLOAD_DIR'], f.filename)
  f.save(path)
  response = jsonify(result='outputs?f=' +  f.filename)
  return response

# def test_static()
#   return 
if __name__=="__main__":
  app.run(debug=True)
  # host 등을 직접 지정하고 싶다면
  # app.run(host="127.0.0.1", port="5000", debug=True)